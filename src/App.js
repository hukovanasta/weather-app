import React from 'react';
import PublickRoutes from './Routes/PublickRoutes';
import LayoutContent from './Components/Layout/LayoutContent';

function App() {
  return (
    <div>
     <LayoutContent>
      <PublickRoutes />
    </LayoutContent>
    </div>
  )
}

export default App;

