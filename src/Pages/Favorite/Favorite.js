import React from 'react';
import { Row, Col } from 'antd';
import {useSelector} from 'react-redux';
import WeatherCard from "../../Components/WeatherCard/WeatherCard";


const Favorite = () => {
    const { favoriteForecasts } = useSelector((state) => state.favorites)

    return (
        <div className="weather-card-wrap" style={{margin: '40px 0px 0px 0px'}}>
          <Row justify="center" gutter={12} >
            {favoriteForecasts.map((favorite, index) =>
              (
                  <Col key={index}>
                      <WeatherCard
                          searchedForecast={favorite.weather}
                          city={favorite.city.name}
                          currentCityKey={favorite.city.key}
                      />
                  </Col>
              ))
            }
          </Row>
        </div>
    );
};

export default Favorite;
