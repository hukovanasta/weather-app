import React, { useEffect } from "react";
import styled from 'styled-components';
import Search from '../../Components/Search/Search';
import WeatherCard from '../../Components/WeatherCard/WeatherCard';
import {Alert, Space} from 'antd';
import ForecastTitle from '../../Components/ForecastTitle/ForecastTitle';
import { Row } from 'antd';
import ForecastList from '../../Components/ForecastList/ForecastList';
import { getCityWeatherByKey } from '../../Store/Actions/WeatherActions';
import {useDispatch, useSelector} from "react-redux";
import {setSelectedCity} from "../../Store/Actions/CitiesActions";
import FavoriteButton from '../../Components/FavoriteBatton/FavoriteButton';
const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 30px;
`;

const DEFAULT_CITY_KEY = "215854"
const DEFAULT_CITY_NAME = "Tel Aviv"

const Home = () => {
  const dispatch = useDispatch();
  const { searchedForecast } = useSelector(state => state.weather);
  const { selectedCity } = useSelector(state => state.cities);

  useEffect(() => {
    dispatch(setSelectedCity({ name: DEFAULT_CITY_NAME, key: DEFAULT_CITY_KEY }))
    dispatch(getCityWeatherByKey(DEFAULT_CITY_KEY))
  }, [])

  const onSearchSelect = (selectedCity) => {
      dispatch(getCityWeatherByKey(selectedCity.value))
  }

  return (
    <Wrapper>
      <Search onSearchSelect={onSearchSelect} />
        { selectedCity ? (
            <>
              <div>
                <Row justify="space-between" gutter={12} >
                    <div style={{margin: '20px 0px 0px 172px'}}>
                        <WeatherCard searchedForecast={searchedForecast} city={selectedCity.name} currentCityKey={selectedCity.key}/>
                    </div>
                    {/* <Space size={10}>
                        <FavoriteButton  />
                    </Space> */}
                </Row>
              </div>
              <div>
               <ForecastTitle/>
              </div>
              <div>
                <ForecastList searchedForecast={searchedForecast}/>
              </div>
            </>
        ) : (
            <div style={{width: "100%"}}>
                <Alert message="No city selected" type="info" />
            </div>
        ) }
    </Wrapper>
  );
};

export default Home;

