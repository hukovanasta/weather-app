import axios from 'axios';

const api  = axios.create({
  baseURL: `http://dataservice.accuweather.com/`,
  params: {
    apikey: 'yPu77kXYByuhCrrRLTdrrNqPQmJKi1WO',
    language: 'en-us'
  }
});

export default api;
