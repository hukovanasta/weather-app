import React, {useMemo} from 'react';
import { Button } from 'antd';
import {useDispatch, useSelector} from "react-redux";
import {addToFavorite, removeFromFavorite} from "../../Store/Actions/FavouritesActions";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash} from '@fortawesome/free-solid-svg-icons';
import {  faHeart } from '@fortawesome/free-solid-svg-icons';



const FavoriteButton = ({ currentCityKey }) => {
    const dispatch = useDispatch()
    const { favoriteForecasts } = useSelector((store) => store.favorites)
    const { searchedForecast } = useSelector(state => state.weather);
    const { selectedCity } = useSelector(state => state.cities);

    const isCityExist = useMemo(
        () => favoriteForecasts.some(item => item.city.key === currentCityKey),
        [favoriteForecasts, currentCityKey]
    )

    const handleButtonClick = () => {
        if (isCityExist) {
            dispatch(removeFromFavorite(currentCityKey))
            return
        }

        dispatch(addToFavorite({
            weather: JSON.parse(JSON.stringify(searchedForecast)),
            city: { ...selectedCity },
        }))
    }

    return (
        <div className="container">
          <Button  style={{border: 'none', position: 'absolute', top: '20px', left: '380px'}} ghost="true" size="large" onClick={handleButtonClick}>
              {
                  isCityExist ?  <FontAwesomeIcon icon={faTrash} size="2x" style={{ color: '5467b8' }} /> :   <FontAwesomeIcon icon={ faHeart } spin  size="2x" style={{ color: '#dea310' }} />
              }
          </Button>
        </div>
    );
}

export default FavoriteButton;